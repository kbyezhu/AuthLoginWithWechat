<?php

namespace addons\login;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Login extends Addons
{

    /**
     * 插件安装方法（只需要安装用户表即可）
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }

    /**
     * 插件启用方法
     */
    public function enable()
    {
        return true;
    }

    /**
     * 插件禁用方法
     */
    public function disable()
    {
        return true;
    }

}
