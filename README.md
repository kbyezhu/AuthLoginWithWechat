#  Fastadmin微信小程序授权登录插件

#### 介绍
> 旨在方便自己日后做这方面的开发，直接安装就好，省事.本项目是基于2021年4月13日微信小程序授权登录做的整改进行开发的插件

#### 软件架构

> 毫无架构可言


#### 使用说明

1.  Install_Package目录 是打包过的插件安装包，可以直接用于安装
2.  接口文档在下方已经贴出
3.  前端测试demo:在application/api/minapp_v2 里面，使用时只需要修改minapp_v2/utils/api.js 里面的 http地址即可
4.  使用前还记得要把插件管理里面的配置项的小程appid跟密钥配置好

![输入图片说明](https://images.gitee.com/uploads/images/2021/0416/112431_447b3d59_968332.png "屏幕截图.png")
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



![输入图片说明](https://images.gitee.com/uploads/images/2021/0416/121459_d06624ef_968332.png "屏幕截图.png")



## 下面是API文档

##### API一简要描述

- code换取openid接口

##### 请求URL
- `/api/Auth_login_with_wechat/code`

##### 请求方式
- GET/POST

##### 参数

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|code |是  |int |wx.login 获取的code   |

##### 返回示例

``` 
{
    "code": 1,
    "msg": "成功",
    "time": "1618534098",
    "data": {
        "openid": "o5rIS5MzaMjZBz3DMzUtsosMZApU",
        "session_key": "z5kYsGmcb7MaRcS0a1AGPQ=="
    }
}

{
    "code": 0,
    "msg": "code been used, hints: [ req_id: YDHBA5yFe-i1q0RA ]",
    "time": "1618534253",
    "data": null
}
```

##### 返回参数说明

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|code |int   |状态码，1：成功；0：失败  |
|msg |string   |提示语  |
|openid |string   |openid  用于后面的登录传值 |
|session_key |string   |session_key 用于后面的登录传值 |




##### API二简要描述

- 登录接口

##### 请求URL
- `/api/Auth_login_with_wechat/login`

##### 请求方式
- POST

##### 参数

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|encryptedData |是  |string |加密串   |
|iv |是  |string |偏移量   |
|sessionKey |是  |string |sessionKey   |
|openid |是  |string |openid   |

##### 返回示例

``` 
{
    "code": 1,
    "msg": "登录成功",
    "time": "1618535634",
    "data": {
        "id": 2,
        "username": "",
        "nickname": "Blustant'C 吴伟祥 可乐爸爸",
        "mobile": "",
        "avatar": "https://thirdwx.qlogo.cn/mmopen/vi_32/Hlq7zKcsbIXveqiaEuRI6SHbk1zSBADsf6SVMWI9VxRxmpz4R917QHfcFTlsLFC8IVhZ418LVlv3DX4VjzyKibZg/132",
        "score": 0,
        "token": "47b64df8-d5ee-43e7-9168-889ea48c11fb",
        "user_id": 2,
        "createtime": 1618535634,
        "expiretime": 1621127634,
        "expires_in": 2592000
    }
}
```

##### 返回参数说明

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|code |int   |状态码，1：成功；0：失败  |
|msg |string   |提示语  |
|token |string   |用于接口请求使用  |





> 解密后的数据格式，并没有openid跟unionID

```angular2html
{
    "nickName":"Blustant'C 吴伟祥 可乐爸爸",
    "gender":1,
    "language":"zh_CN",
    "city":"Hefei",
    "province":"Anhui",
    "country":"China",
    "avatarUrl":"https://thirdwx.qlogo.cn/mmopen/vi_32/Hlq7zKcsbIXveqiaEuRI6SHbk1zSBADsf6SVMWI9VxRxmpz4R917QHfcFTlsLFC8IVhZ418LVlv3DX4VjzyKibZg/132",
    "watermark":{
        "timestamp":1618498216,
        "appid":"wx9d9766d500e04dcd"
    }
}
```

