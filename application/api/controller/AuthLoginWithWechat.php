<?php
/**
 * Created by 老吴.
 * UserMsg:砥砺前行，扬帆起航
 * email:cwwx0128@qq.com
 * QQ:1113249273
 * QQ群:925283872
 * 微信:cww0128
 * Date: 2021/4/15
 * Time: 23:08
 */

namespace app\api\controller;

use app\common\controller\Api;
use fast\Http;

class AuthLoginWithWechat extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $appid;
    protected $secret;
    protected $loginUrl;

    public function __construct()
    {
        $this->appid = get_addon_config("login")['appId'];
        $this->secret = get_addon_config("login")['secretId'];
        $this->loginUrl = get_addon_config("login")['loginUrl'];
        parent::__construct();
    }

    //https://hongqi-b.maoln.com/api/Auth_login_with_wechat/test

    /**
     * 登录第一步，获取openid 跟 session_key
     */
    public function code()
    {
        $code = $this->request->param('code');
        if (!$code) {
            $this->error('code不能为空');
        }
        self::getOpenid($code);
    }


    /**
     * @param $code 用来交换获取openid 跟 session_key
     */
    public function getOpenid($code)
    {
        $url = sprintf($this->loginUrl, $this->appid, $this->secret, $code);
        $result = Http::get($url);
        $wxResult = json_decode($result, true);
        if (empty($wxResult)) {
            $this->error('获取sessin_key及openID时异常');
        }
        if (isset($wxResult['errcode']) && $wxResult['errcode'] != 0) {
            $this->error($wxResult['errmsg']);
        }
        $item = [
            'openid' => $wxResult['openid'],
            'session_key' => $wxResult['session_key']
        ];
        $this->success('成功', $item);
    }


    /**
     * 用户登录
     */
    public function login()
    {
        $encryptedData = $this->request->post('encryptedData');
        $iv = $this->request->post('iv');
        $sessionKey = $this->request->post('sessionKey');
        $openid = $this->request->post('openid');

        if (empty($encryptedData) || empty($iv) || empty($sessionKey) || empty($openid)) {
            $this->error('缺少参数');
        }
        $errCode = self::decryptData($encryptedData, $iv, $data, $sessionKey, $this->appid);
        if ($errCode == 0) {
            $result = json_decode($data, true);

            $userinfo = \app\admin\model\User::where(['openid' => $openid])->find();

//            $ah = new Auth();

            if ($userinfo) {
                $userinfo->nickname = $result['nickName'];
                $userinfo->avatar = $result['avatarUrl'];
                $userinfo->gender = $result['gender'];
                $userinfo->city = $result['city'];
                $userinfo->province = $result['province'];
                $userinfo->country = $result['country'];
                $userinfo->save();
                $this->auth->direct($userinfo['id']);
            } else {
                $user = new \app\admin\model\User();
                $user->data([
                    'nickname' => $result['nickName'],
                    'avatar' => $result['avatarUrl'],
                    'gender' => $result['gender'],
                    'city' => $result['city'],
                    'province' => $result['province'],
                    'country' => $result['country'],
                    'status' => 'normal',
                    'openid' => $openid
                ]);
                $user->save();
                $this->auth->direct($user->id);
            }
            $this->success('登录成功', $this->auth->getUserinfo());
        } else {
            $this->error('登录失败' . $errCode);
        }
    }

    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData($encryptedData, $iv, &$data, $sessionKey, $appid)
    {
        if (strlen($sessionKey) != 24) {
            return -41001;
        }
        $aesKey = base64_decode($sessionKey);


        if (strlen($iv) != 24) {
            return -41002;
        }
        $aesIV = base64_decode($iv);

        $aesCipher = base64_decode($encryptedData);

        $result = openssl_decrypt($aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);

        $dataObj = json_decode($result);
        if ($dataObj == NULL) {
            return -41003;
        }
        if ($dataObj->watermark->appid != $appid) {
            return -41003;
        }
        $data = $result;
        return 0;
    }


}